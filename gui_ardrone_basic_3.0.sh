#!/bin/bash

#---------------------------------------------------------------------------------------------
# FILE:   gui_simulated_quadrotor_basic.sh
#
# BRIEF:  Launches the gui for a generic simulated quadrotor
#
# DETAIL: Includes the following configuration of processes:
#         - Human machine interface
#---------------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------
# Input arguments
#---------------------------------------------------------------------------------------------
NUMID_DRONE=$1
NETWORK_ROSCORE=$2
DRONE_IP=$3
DRONE_WCHANNEL=$4

#---------------------------------------------------------------------------------------------
# Default values for arguments
#
# This code checks the existence of arguments as it is explained at:
#      http://stackoverflow.com/questions/6482377/bash-shell-script-check-input-argument
#---------------------------------------------------------------------------------------------
if [ -z $NETWORK_ROSCORE ] # Check if NETWORK_ROSCORE is NULL
  then
    # Argument 2 is empty
    . ${AEROSTACK_STACK}/setup.sh
    OPEN_ROSCORE=1
  else
   . ${AEROSTACK_STACK}/setup.sh $2
fi

if [ -z $NUMID_DRONE ] # Check if NUMID_DRONE is NULL
  then
    # Argument 1 empty
    echo "-Setting droneId = 1"
    NUMID_DRONE=1
  else
    echo "-Setting droneId = $1"
fi

if [ -z $DRONE_IP ] # Check if NUMID_DRONE is NULL
  then
    # Argument 3 is empty
    echo "-Setting droneIp = 192.168.1.1"
    DRONE_IP=192.168.1.1
  else
    echo "-Setting droneIp = $3"
  fi

if [ -z $DRONE_WCHANNEL ] # Check if NUMID_DRONE is NULL
  then
    # Argument 4 is empty
    echo "-Setting droneChannel = 6"
    DRONE_WCHANNEL=6
  else
    echo "-Setting droneChannel = $4"
  fi
#---------------------------------------------------------------------------------------------
# USER INTERFACE PROCESSES
#---------------------------------------------------------------------------------------------
xfce4-terminal  \
`#---------------------------------------------------------------------------------------------` \
`# Graphical User Interface                                                                               ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "GUI"  --command "bash -c \"
roslaunch main_window gui.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE  \
    drone_pose_subscription:=estimated_pose \
    drone_speeds_subscription:=estimated_speeds \
    simulated:=n \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Behavior Specialist                                                                         ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior Specialist" --command "bash -c \"
roslaunch behavior_specialist_process behavior_specialist_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  &
