#!/bin/bash

NUMID_DRONE=$1
NETWORK_ROSCORE=$2
DRONE_IP=$3
# http://stackoverflow.com/questions/6482377/bash-shell-script-check-input-argument
if [ -z $NETWORK_ROSCORE ] # Check if NETWORK_ROSCORE is NULL
  then
  	#Argument 2 is empty
	. ${AEROSTACK_STACK}/setup.sh
    	OPEN_ROSCORE=1
  else
    	. ${AEROSTACK_STACK}/setup.sh $2
fi
if [ -z $NUMID_DRONE ] # Check if NUMID_DRONE is NULL
  then
  	#Argument 1 empty
    	echo "-Setting droneId = 5"
    	NUMID_DRONE=3
  else
    	echo "-Setting droneId = $1"
fi
if [ -z $DRONE_IP ] # Check if NUMID_DRONE is NULL
  then
  	#Argument 3 is empty
    	echo "-Setting droneIp = 192.168.1.1"
    	DRONE_IP=192.168.1.1
  else
    	echo "-Setting droneIp = $3"
fi

`
#---------------------------------------------------------------------------------------------` \
`# Process monitor                                                                             ` \
`#---------------------------------------------------------------------------------------------` \
gnome-terminal  \
--tab --title "python based mission interpreter" --command "bash -c \"
roslaunch python_based_mission_interpreter_process python_based_mission_interpreter_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    mission:="mission3";
exec bash\""  \
--tab --title "behavior_coordinator_process"	--command "bash -c \"
roslaunch behavior_coordinator_process behavior_coordinator_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  \
--tab --title "behavior_specialist_process"	--command "bash -c \"
roslaunch behavior_specialist_process behavior_specialist_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    executive_layer_configuration:=/configs/drone$NUMID_DRONE/catalog_imav_mission3.yaml;
exec bash\""  \
--tab --title "resource_manager_process"	--command "bash -c \"
roslaunch resource_manager_process resource_manager_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  \
--tab --title "belief_updater"  --command "bash -c \"
roslaunch belief_updater_process belief_updater_process.launch  --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  \
--tab --title "belief_manager"  --command "bash -c \"
roslaunch belief_manager_process belief_manager_process.launch  --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  \
--tab --title "belief_manager"  --command "bash -c \"
roslaunch self_localization_selector_process self_localization_selector_process.launch  --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  \
--tab --title "behavior_take_off"  --command "bash -c \"
roslaunch behavior_take_off behavior_take_off.launch  --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    estimated_pose_topic:=EstimatedPose_droneGMR_wrt_GFF \
    estimated_speed_topic:=EstimatedSpeed_droneGMR_wrt_GFF;	
exec bash\""  \
--tab --title "behavior_land"  --command "bash -c \"
roslaunch behavior_land behavior_land.launch  --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    estimated_pose_topic:=EstimatedPose_droneGMR_wrt_GFF \
    estimated_speed_topic:=EstimatedSpeed_droneGMR_wrt_GFF;
exec bash\""  \
--tab --title "behavior_move_to_visual_marker"  --command "bash -c \"
roslaunch behavior_move_to_visual_marker behavior_move_to_visual_marker.launch  --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    estimated_pose_topic:=EstimatedPose_droneGMR_wrt_GFF \
    estimated_speed_topic:=EstimatedSpeed_droneGMR_wrt_GFF;
exec bash\""  \
--tab --title "behavior_land_on_static_platform"  --command "bash -c \"
roslaunch behavior_land_on_static_platform behavior_land_on_static_platform.launch  --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    estimated_pose_topic:=EstimatedPose_droneGMR_wrt_GFF;
exec bash\""  \
--tab --title "behavior_pay_attention_to_visual_markers"  --command "bash -c \"
roslaunch behavior_pay_attention_to_visual_markers behavior_pay_attention_to_visual_markers.launch  --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    estimated_pose_topic:=EstimatedPose_droneGMR_wrt_GFF \
    estimated_speed_topic:=EstimatedSpeed_droneGMR_wrt_GFF;
exec bash\"" \ &
