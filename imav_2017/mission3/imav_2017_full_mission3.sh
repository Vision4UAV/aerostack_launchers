#!/bin/bash

gnome-terminal  \
	--tab --command "bash -c \"
sleep 5
cd ${AEROSTACK_WORKSPACE};
source devel/setup.bash;
cd ${AEROSTACK_STACK}/launchers/imav_2017/mission3/visual_servo; 
./imav_2017_mission3.sh;
	exec bash\""  \
	--tab --command "bash -c \"
sleep 5
cd ${AEROSTACK_WORKSPACE};
source devel/setup.bash;
cd ${AEROSTACK_STACK}/launchers/imav_2017/mission3/visual_servo;  
./imav_2017_mission3_planner.sh;
	exec bash\""  \ &
