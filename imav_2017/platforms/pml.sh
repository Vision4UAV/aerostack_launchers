ROS_TITLE[$index]="Pml_Mission_Interpreter_Process"
ROS_COMMAND[$index]="\
roslaunch python_based_mission_interpreter_process python_based_mission_interpreter_process.launch --wait \
    mission:=mission.py drone_id_int:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))
