DRONE_WTYPE=1

ROS_TITLE[$index]="Bebop Autonomy"
ROS_COMMAND[$index]=\
"roslaunch ${AEROSTACK_STACK}/launchers/bebop_launchers/launch_files/bebop_node.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    drone_ip:=$DRONE_IP \
    my_stack_directory:=${AEROSTACK_STACK} \
    wifi_selection_type:=${DRONE_WTYPE}"
((index++))

ROS_TITLE[$index]="Driver Bebop"
ROS_COMMAND[$index]=\
"roslaunch driverBebopROSModule driverBebopROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    drone_ip:=$DRONE_IP \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="Robot localization"
ROS_COMMAND[$index]=\
"roslaunch ${AEROSTACK_STACK}/launchers/bebop_launchers/launch_files/EKF.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    drone_ip:=$DRONE_IP \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="Robot localizationROSModule"
ROS_COMMAND[$index]=\
"roslaunch droneRobotLocalizationROSModule droneRobotLocalizationROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    drone_ip:=$DRONE_IP \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))
