ROS_TITLE[$index]="Trajectory_Controller"
ROS_COMMAND[$index]=\
"roslaunch droneTrajectoryControllerROSModule droneTrajectoryControllerROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    drone_estimated_pose_topic_name:=${POSE_TOPIC} \
    drone_estimated_speeds_topic_name:=${SPEED_TOPIC}"
((index++))

ROS_TITLE[$index]="Obstacle_Distance_Calculator"
ROS_COMMAND[$index]=\
"roslaunch droneObstacleDistanceCalculatorROSModule droneObstacleDistanceCalculationROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK} \
    drone_pose_topic_name:=${POSE_TOPIC}"
((index++))

ROS_TITLE[$index]="Obstacle_Processor"
ROS_COMMAND[$index]=\
"roslaunch droneObstacleProcessorVisualMarksROSModule droneObstacleProcessor2dVisualMarksROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="Visual_Marker_Localizer"
ROS_COMMAND[$index]=\
"roslaunch droneVisualMarkersLocalizerROSModule droneVisualMarkersLocalizerROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="Tracker_Eye"
ROS_COMMAND[$index]=\
"roslaunch droneTrackerEyeROSModule droneTrackerEyeROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="Aruco_Eye"
ROS_COMMAND[$index]=\
"roslaunch drone_aruco_eye_ros_module droneArucoEyeROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    camera_calibration_file:=bebop2_cam_calib_480.yaml"
((index++))

ROS_TITLE[$index]="Trajectory_Planner"
ROS_COMMAND[$index]=\
"roslaunch droneTrajectoryPlannerROSModule droneTrajectoryPlanner2dROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK} \
    drone_pose_topic_name:=${POSE_TOPIC}"
((index++))

ROS_TITLE[$index]="Yaw_Commander"
ROS_COMMAND[$index]=\
"roslaunch droneYawCommanderROSModule droneYawCommanderROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    drone_pose_topic_name:=${POSE_TOPIC}"
((index++))

ROS_TITLE[$index]="Communication_Manager"
ROS_COMMAND[$index]=\
"roslaunch droneCommunicationManagerROSModule droneCommunicationManagerROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    estimated_pose_topic_name:=${POSE_TOPIC}"
((index++))

ROS_TITLE[$index]="Process_Monitor"
ROS_COMMAND[$index]=\
"roslaunch process_monitor_process process_monitor.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="QR Recognizer"
ROS_COMMAND[$index]=\
"roslaunch qr_recognizer qr_recognizer.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))
