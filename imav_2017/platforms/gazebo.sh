fuser -k 14550/udp 14556/udp &>/dev/null

ROS_TITLE[$index]="Mavros Simulation"
ROS_COMMAND[$index]="roslaunch ${AEROSTACK_STACK}/launchers/pixhawk_simulation_launchers/launch_files/px4_SITL.launch  --wait drone_id_int:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK}"
((index++))

# ROS_TITLE[$index]="RobotLocalizationROSModule"
# ROS_COMMAND[$index]="roslaunch droneRobotLocalizationROSModule droneRobotLocalizationROSModule.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK}"
# ((index++))
#
# ROS_TITLE[$index]="Robot localization"
# ROS_COMMAND[$index]="roslaunch ${AEROSTACK_STACK}/launchers/pixhawk_simulation_launchers/launch_files/Ekf.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK}"
# ((index++))

ROS_TITLE[$index]="Driver Pixhawk"
ROS_COMMAND[$index]="roslaunch driverPixhawkROSModule driverPixhawkROSModuleSim.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="Midlevel Controller"
ROS_COMMAND[$index]="roslaunch droneMidLevelAutopilotROSModule droneMidLevelAutopilotROSModule.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK}"
((index++))
