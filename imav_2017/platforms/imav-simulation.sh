ROS_TITLE[$index]="Robot localization"
ROS_COMMAND[$index]=\
"roslaunch ${AEROSTACK_STACK}/launchers/bebop_launchers/launch_files/EKF.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    drone_ip:=$DRONE_IP \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="Robot localizationROSModule"
ROS_COMMAND[$index]=\
"roslaunch droneRobotLocalizationROSModule droneRobotLocalizationROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    drone_ip:=$DRONE_IP \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="Driver IMAV 2017"
ROS_COMMAND[$index]=\
"roslaunch driver_imav_2017_sim driver_imav_2017_sim.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="Hector SLAM"
ROS_COMMAND[$index]=\
"roslaunch ${AEROSTACK_STACK}/stack_devel/imav_2017_simulation/launch_files/hector_slam.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))
