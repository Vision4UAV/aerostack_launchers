ROS_TITLE[$index]="sound_play"
ROS_COMMAND[$index]=\
"roslaunch ${AEROSTACK_STACK}/launchers/sound_play.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))


ROS_TITLE[$index]="DroneSpeechModule"
ROS_COMMAND[$index]=\
"roslaunch droneSpeechROSModule droneSpeechROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    voice:=voice_el_diphone \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="DroneSoundModule"
ROS_COMMAND[$index]=\
"roslaunch droneSoundROSModule droneSoundROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    voice:=voice_el_diphone \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))
