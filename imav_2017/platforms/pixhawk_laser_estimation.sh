ROS_TITLE[$index]="RobotLocalizationROSModule"
ROS_COMMAND[$index]="roslaunch droneRobotLocalizationROSModule droneRobotLocalizationROSModule.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="Robot localization"
ROS_COMMAND[$index]="roslaunch ${AEROSTACK_STACK}/launchers/pixhawk_simulation_launchers/launch_files/Ekf.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="Laser Based Pose Estimation Module"
ROS_COMMAND[$index]="roslaunch laser_based_pose_estimation droneLaserBasedPoseEstimationROSModule.launch --wait drone_id_namespace:=drone$NUMID_DRONE drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK}"
((index++))
