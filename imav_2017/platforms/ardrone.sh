MAC_ADDRESS="9C:B6:D0:1B:68:53"

ROS_TITLE[$index]="ArDrone_Autonomy"
ROS_COMMAND[$index]=\
"roslaunch ${AEROSTACK_STACK}/launchers/ardrone_launch/ardrone_indoors.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    drone_ip:=$DRONE_IP \
    my_stack_directory:=${AEROSTACK_STACK} \
    owner_mac:=${MAC_ADDRESS}"
((index++))

ROS_TITLE[$index]="Driver_Parrot"
ROS_COMMAND[$index]=\
"roslaunch driverParrotARDroneROSModule driverParrotARDroneROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="State_Estimator"
ROS_COMMAND[$index]=\
"roslaunch droneEKFStateEstimatorROSModule droneEKFStateEstimatorROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))
