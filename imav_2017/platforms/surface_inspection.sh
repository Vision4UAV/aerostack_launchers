ROS_TITLE[$index]="User command line interpreter"
ROS_COMMAND[$index]="$TERMINAL_COMMAND_NO_QUIT \
$ROSLAUNCH_PATH user_command_line_interpreter_process user_command_line_interpreter_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="Surface Inspection"
ROS_COMMAND[$index]=\
"roslaunch surface_inspection_process surface_inspection_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))
