ROS_TITLE[$index]="Belief_Manager_Process"
ROS_COMMAND[$index]="\
roslaunch belief_manager_process belief_manager_process.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Belief_Updater_Process"
ROS_COMMAND[$index]="\
roslaunch belief_updater_process belief_updater_process.launch --wait\
    pose_topic:=${POSE_TOPIC} \
    drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))
