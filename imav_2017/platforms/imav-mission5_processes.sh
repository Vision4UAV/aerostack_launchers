# . $AEROSTACK_STACK/launchers/imav_2017/platforms/bebop.sh
DRONE_WTYPE=1
ROS_TITLE[$index]="Bebop Autonomy"
ROS_COMMAND[$index]=\
"roslaunch ${AEROSTACK_STACK}/launchers/bebop_launchers/launch_files/bebop_node.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    drone_ip:=$DRONE_IP \
    my_stack_directory:=${AEROSTACK_STACK} \
    wifi_selection_type:=${DRONE_WTYPE}"
((index++))

ROS_TITLE[$index]="Driver Bebop"
ROS_COMMAND[$index]=\
"roslaunch driverBebopROSModule driverBebopROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    drone_ip:=$DRONE_IP \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="Robot localization"
ROS_COMMAND[$index]=\
"roslaunch ${AEROSTACK_STACK}/launchers/bebop_launchers/launch_files/EKF.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    drone_ip:=$DRONE_IP \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="Robot localizationROSModule"
ROS_COMMAND[$index]=\
"roslaunch droneRobotLocalizationROSModule droneRobotLocalizationROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    drone_ip:=$DRONE_IP \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))


#. $AEROSTACK_STACK/launchers/imav_2017/platforms/behavior_library.sh
ROS_TITLE[$index]="Behavior_Take_Off"
ROS_COMMAND[$index]="\
roslaunch behavior_take_off behavior_take_off.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior Move Pitch"
ROS_COMMAND[$index]="\
roslaunch behavior_move_pitch behavior_move_pitch.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

# . $AEROSTACK_STACK/launchers/imav_2017/platforms/behavior.sh
ROS_TITLE[$index]="Behavior_Coordinator"
ROS_COMMAND[$index]="\
roslaunch behavior_coordinator_process behavior_coordinator_process.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior_Specialist"
ROS_COMMAND[$index]="\
roslaunch behavior_specialist_process behavior_specialist_process.launch --wait \
    drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE\
    executive_layer_configuration:=/configs/drone$NUMID_DRONE/catalog_imav.yaml"
((index++))

ROS_TITLE[$index]="Resource_Manager"
ROS_COMMAND[$index]="\
roslaunch resource_manager_process resource_manager_process.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Self_Localization_Selector"
ROS_COMMAND[$index]="\
roslaunch self_localization_selector_process self_localization_selector_process.launch --wait \
      drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))


# . $AEROSTACK_STACK/launchers/imav_2017/platforms/common.sh
ROS_TITLE[$index]="Trajectory_Controller"
ROS_COMMAND[$index]=\
"roslaunch droneTrajectoryControllerROSModule droneTrajectoryControllerROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    drone_estimated_pose_topic_name:=${POSE_TOPIC} \
    drone_estimated_speeds_topic_name:=${SPEED_TOPIC}"
((index++))

ROS_TITLE[$index]="Obstacle_Distance_Calculator"
ROS_COMMAND[$index]=\
"roslaunch droneObstacleDistanceCalculatorROSModule droneObstacleDistanceCalculationROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK} \
    drone_pose_topic_name:=${POSE_TOPIC}"
((index++))

ROS_TITLE[$index]="Obstacle_Processor"
ROS_COMMAND[$index]=\
"roslaunch droneObstacleProcessorVisualMarksROSModule droneObstacleProcessor2dVisualMarksROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="Visual_Marker_Localizer"
ROS_COMMAND[$index]=\
"roslaunch droneVisualMarkersLocalizerROSModule droneVisualMarkersLocalizerROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="Tracker_Eye"
ROS_COMMAND[$index]=\
"roslaunch droneTrackerEyeROSModule droneTrackerEyeROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))

ROS_TITLE[$index]="Aruco_Eye"
ROS_COMMAND[$index]=\
"roslaunch drone_aruco_eye_ros_module droneArucoEyeROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    camera_calibration_file:=bebop2_cam_calib_480.yaml"
((index++))

ROS_TITLE[$index]="Trajectory_Planner"
ROS_COMMAND[$index]=\
"roslaunch droneTrajectoryPlannerROSModule droneTrajectoryPlanner2dROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE my_stack_directory:=${AEROSTACK_STACK} \
    drone_pose_topic_name:=${POSE_TOPIC}"
((index++))

ROS_TITLE[$index]="Yaw_Commander"
ROS_COMMAND[$index]=\
"roslaunch droneYawCommanderROSModule droneYawCommanderROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    drone_pose_topic_name:=${POSE_TOPIC}"
((index++))

ROS_TITLE[$index]="Communication_Manager"
ROS_COMMAND[$index]=\
"roslaunch droneCommunicationManagerROSModule droneCommunicationManagerROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK} \
    estimated_pose_topic_name:=${POSE_TOPIC}"
((index++))

ROS_TITLE[$index]="Process_Monitor"
ROS_COMMAND[$index]=\
"roslaunch process_monitor_process process_monitor.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_STACK}"
((index++))


# . $AEROSTACK_STACK/launchers/imav_2017/platforms/belief.sh
ROS_TITLE[$index]="Belief_Manager_Process"
ROS_COMMAND[$index]="\
roslaunch belief_manager_process belief_manager_process.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Belief_Updater_Process"
ROS_COMMAND[$index]="\
roslaunch belief_updater_process belief_updater_process.launch --wait\
    pose_topic:=${POSE_TOPIC} \
    drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))


# . $AEROSTACK_STACK/launchers/imav_2017/platforms/pml.sh
ROS_TITLE[$index]="Pml_Mission_Interpreter_Process"
ROS_COMMAND[$index]="\
roslaunch python_based_mission_interpreter_process python_based_mission_interpreter_process.launch --wait \
    mission:=mission.py drone_id_int:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))
