ROS_TITLE[$index]="Behavior_Coordinator"
ROS_COMMAND[$index]="\
roslaunch behavior_coordinator_process behavior_coordinator_process.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior_Specialist"
ROS_COMMAND[$index]="\
roslaunch behavior_specialist_process behavior_specialist_process.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Resource_Manager"
ROS_COMMAND[$index]="\
roslaunch resource_manager_process resource_manager_process.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Self_Localization_Selector"
ROS_COMMAND[$index]="\
roslaunch self_localization_selector_process self_localization_selector_process.launch --wait \
      drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))
