ROS_TITLE[$index]="Controller IBVS"
ROS_COMMAND[$index]="\
roslaunch controller_IBVS controller_IBVS.launch --wait drone_id_int:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE my_stack_directory:=$AEROSTACK_STACK"
((index++))

ROS_TITLE[$index]="Perception IBVS"
ROS_COMMAND[$index]="\
roslaunch perception_IBVS perception_IBVS.launch --wait drone_id_int:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE my_stack_directory:=$AEROSTACK_STACK"
((index++))

ROS_TITLE[$index]="State_Estimator_IBVS"
ROS_COMMAND[$index]="\
roslaunch state_estimator_IBVS state_estimator_IBVS.launch --wait drone_id_int:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE my_stack_directory:=$AEROSTACK_STACK"
((index++))

ROS_TITLE[$index]="Speed Controller"
ROS_COMMAND[$index]="\
roslaunch speed_controller speed_controller.launch --wait drone_id_int:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE my_stack_directory:=$AEROSTACK_STACK"
((index++))
