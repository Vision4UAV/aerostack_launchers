ROS_TITLE[$index]="Behavior_Take_Off"
ROS_COMMAND[$index]="\
roslaunch behavior_take_off behavior_take_off.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior_Keep_Hovering"
ROS_COMMAND[$index]="\
roslaunch behavior_keep_hovering behavior_keep_hovering.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior_Land"
ROS_COMMAND[$index]="\
roslaunch behavior_land behavior_land.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior Land On Static Platform"
ROS_COMMAND[$index]="\
roslaunch behavior_land_on_static_platform behavior_land_on_static_platform.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior Drop Object"
ROS_COMMAND[$index]="\
roslaunch behavior_drop_object behavior_drop_object.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior Land On Moving Platform"
ROS_COMMAND[$index]="\
roslaunch behavior_land_on_moving_platform behavior_land_on_moving_platform.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior_Go_To_Point"
ROS_COMMAND[$index]="\
roslaunch behavior_go_to_point behavior_go_to_point.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior_Rotate"
ROS_COMMAND[$index]="\
roslaunch behavior_rotate behavior_rotate.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior_Keep_Moving"
ROS_COMMAND[$index]="\
roslaunch behavior_keep_moving behavior_keep_moving.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior Follow Object Image"
ROS_COMMAND[$index]="\
roslaunch behavior_follow_object_image behavior_follow_object_image.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior Wait"
ROS_COMMAND[$index]="\
roslaunch behavior_wait behavior_wait.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior_Notify_Operator"
ROS_COMMAND[$index]="\
roslaunch behavior_notify_operator behavior_notify_operator.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior Self Localize By Odometry"
ROS_COMMAND[$index]="\
roslaunch behavior_self_localize_by_odometry behavior_self_localize_by_odometry.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior Self Localize By Visual Marker"
ROS_COMMAND[$index]="\
roslaunch behavior_self_localize_by_visual_marker behavior_self_localize_by_visual_marker.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior Pay Attention to Visual Markers"
ROS_COMMAND[$index]="\
roslaunch behavior_pay_attention_to_visual_markers behavior_pay_attention_to_visual_markers.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior Flip"
ROS_COMMAND[$index]="\
roslaunch behavior_flip behavior_flip.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))

ROS_TITLE[$index]="Behavior Request Operator Assistance"
ROS_COMMAND[$index]="\
roslaunch behavior_request_operator_assistance behavior_request_operator_assistance.launch --wait drone_id:=$NUMID_DRONE drone_id_namespace:=drone$NUMID_DRONE"
((index++))
