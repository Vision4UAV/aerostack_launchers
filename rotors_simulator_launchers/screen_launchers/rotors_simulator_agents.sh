#!/bin/bash



DRONE_SWARM_MEMBERS=$1

if [ -z $DRONE_SWARM_MEMBERS ] # Check if NUMID_DRONE is NULL
  then
  	#Argument 1 empty
    	echo "-Setting Swarm Members = 1"
    	DRONE_SWARM_MEMBERS=1
  else
    	echo "-Setting DroneSwarm Members = $1"
fi


for (( c=1; c<=$DRONE_SWARM_MEMBERS; c++ ))
do  

screen -S rotors -X screen -t rotors_simulator_agents_$c ${AEROSTACK_STACK}/launchers/rotors_simulator_launchers/screen_launchers/rotors_simulator_mav.sh $c

done


