#!/bin/bash



DRONE_SWARM_MEMBERS=$1

if [ -z $DRONE_SWARM_MEMBERS ] # Check if NUMID_DRONE is NULL
  then
  	#Argument 1 empty
    	echo "-Setting Swarm Members = 1"
    	DRONE_SWARM_MEMBERS=1
  else
    	echo "-Setting DroneSwarm Members = $1"
fi

cd $AEROSTACK_STACK
source setup.sh ''

roslaunch rotors_gazebo mav_swarm.launch --wait drone_swarm_number:=$DRONE_SWARM_MEMBERS mav_name:=hummingbird




